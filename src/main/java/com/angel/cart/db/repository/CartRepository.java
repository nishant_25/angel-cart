package com.angel.cart.db.repository;

import com.angel.cart.db.entity.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface CartRepository extends JpaRepository<CartEntity, String> {

    @Modifying
    @Transactional
    @Query(value="insert into cart(product_id, user_id) values(?1, ?2)", nativeQuery = true)
    void addProductToCart(String productId, String userId);


    @Modifying
    @Transactional
    @Query(value="delete from cart where product_id=?1 and user_id=?2", nativeQuery = true)
    void removeProductFromCart(String productId, String userId);
}
