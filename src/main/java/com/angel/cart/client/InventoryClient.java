package com.angel.cart.client;

import com.angel.cart.client.dto.InventoryDTO;
import com.angel.cart.utils.JacksonUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
@RequiredArgsConstructor
public class InventoryClient {

    @Value("${inventory.service.endpoint}")
    private String inventoryServiceEndpoint;

    @Value("${inventory.fetch.path}")
    private String inventoryServiceBasePath;

    private final OkHttpClient okHttpClient;
    private final JacksonUtil jacksonUtil;
    final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public InventoryDTO getInventory(String productId){
        try{
            Map<String, String> payload = new HashMap<>();
            payload.put("product_id", productId);
            String payload_json = new ObjectMapper().writeValueAsString(payload);

            RequestBody requestBody = RequestBody.create(JSON, payload_json);

            Request request = new Request.Builder()
                    .url(inventoryServiceEndpoint + inventoryServiceBasePath)
                    .post(requestBody)
                    .build();

            log.info("inventory request : {}", request);
            Response response = okHttpClient.newCall(request).execute();

            if(!response.isSuccessful()){
                log.error("Error while fetching the inventory : " + response);
            }else{
                String responseBody = response.body().string();
                if(StringUtils.isEmpty(responseBody)){
                    log.error("Error while fetching the inventory : " + response);
                }else{
                    return jacksonUtil.convertStringToObject(responseBody, InventoryDTO.class);
                }
            }
            return null;
        }catch(Exception exception){
            log.error("Unexpected exception while fetching the inventory : " , exception);
            return null;
        }
    }

}
