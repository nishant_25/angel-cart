package com.angel.cart.client.dto;

import com.angel.cart.model.InventoryModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryDTO {

    @JsonProperty("status")
    String status;

    @JsonProperty("message")
    String message;

    @JsonProperty("error")
    String error;

    @JsonProperty("data")
    InventoryModel inventoryModel;

}
