package com.angel.cart.controller;

import com.angel.cart.constants.ApiConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiConstants.CART + ApiConstants.API + ApiConstants.VERSION)
@RequiredArgsConstructor
@Slf4j

public class HealthCheckController{

    @GetMapping(value = ApiConstants.HEALTH_CHECK)
    public String healthCheckPath() {

        return "Health Check Passed";
    }
}
