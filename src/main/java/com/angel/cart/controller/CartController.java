package com.angel.cart.controller;

import com.angel.cart.constants.ApiConstants;
import com.angel.cart.request.CartRequest;
import com.angel.cart.response.CartResponse;
import com.angel.cart.service.CartService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiConstants.CART)
@RequiredArgsConstructor
@Slf4j
public class CartController {

    @Autowired
    CartService cartService;

    // Update product to cart
    @PostMapping(value = ApiConstants.API + ApiConstants.VERSION + ApiConstants.UPDATE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CartResponse> updateCart(@RequestBody CartRequest cartRequest){
        return cartService.updateProductToCart(cartRequest);
    }


}
