package com.angel.cart.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CartRequest {

    @JsonProperty("product_id")
    String productId;

    @JsonProperty("user_id")
    String userId;

    @JsonProperty("status")
    int status;

}

