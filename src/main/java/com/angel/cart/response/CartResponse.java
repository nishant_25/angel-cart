package com.angel.cart.response;

import com.angel.cart.db.entity.CartEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CartResponse{

    @JsonProperty("data")
    CartEntity data;

    @JsonProperty("message")
    String message;

    @JsonProperty("error")
    String error;

    @JsonProperty("status")
    Integer statusCode;
}

