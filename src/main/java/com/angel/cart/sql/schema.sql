create database cartdb;

use cartdb;

CREATE TABLE `cart` (
`id` int NOT NULL AUTO_INCREMENT,
`product_id` varchar(100) NOT NULL,
`user_id` varchar(100) NOT NULL,
`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;