package com.angel.cart.constants;

public class ApiConstants {

    public static final String CART = "/cart";
    public static final String API = "/api";
    public static final String VERSION = "/v1";
    public static final String HEALTH_CHECK = "/health";
    public static final String UPDATE = "/update";
}
