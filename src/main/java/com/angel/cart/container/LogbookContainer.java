package com.angel.cart.container;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.zalando.logbook.DefaultHttpLogWriter;
import org.zalando.logbook.DefaultSink;
import org.zalando.logbook.Logbook;
import org.zalando.logbook.StatusAtLeastStrategy;
import org.zalando.logbook.json.JsonHttpLogFormatter;

import static org.zalando.logbook.Conditions.exclude;
import static org.zalando.logbook.Conditions.requestTo;
import static org.zalando.logbook.HeaderFilters.replaceHeaders;

@Component
public class LogbookContainer {
    @Bean
    public Logbook logbook() {
        return Logbook.builder()
                .strategy(new StatusAtLeastStrategy(199))
                .condition(
                        exclude(
                                requestTo("/demographics/api/v1/health-check")))
                .headerFilter(replaceHeaders("sso_token"::equalsIgnoreCase, "***"))
                .headerFilter(replaceHeaders("data"::equalsIgnoreCase, "***"))
                .headerFilter(replaceHeaders("Authorization"::equalsIgnoreCase, "***"))
                .sink(new DefaultSink(new JsonHttpLogFormatter(), new DefaultHttpLogWriter()))
                .build();
    }
}
