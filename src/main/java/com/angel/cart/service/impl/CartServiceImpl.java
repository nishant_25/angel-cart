package com.angel.cart.service.impl;

import com.angel.cart.client.InventoryClient;
import com.angel.cart.db.repository.CartRepository;
import com.angel.cart.request.CartRequest;
import com.angel.cart.response.CartResponse;
import com.angel.cart.service.CartService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@NoArgsConstructor
@Slf4j
public class CartServiceImpl implements CartService {

    @Autowired
    CartRepository cartRepository;

    @Autowired
    InventoryClient inventoryClient;

    @Override
    public ResponseEntity<CartResponse> updateProductToCart(CartRequest cartRequest) {
        try {
            CartResponse cartResponse = new CartResponse();
            if (cartRequest.getStatus() == 1) {
                // Do inventory check for the product, make a call to inventory service
                if (!Objects.isNull(inventoryClient.getInventory(cartRequest.getProductId())) && inventoryClient.getInventory(cartRequest.getProductId()).getInventoryModel().getAvailableInventory() > 0) {
                    cartRepository.addProductToCart(cartRequest.getProductId(), cartRequest.getUserId());
                } else {
                    cartResponse.setData(null);
                    cartResponse.setMessage("Sorry this product is out of stock");
                    cartResponse.setStatusCode(200);
                    return new ResponseEntity<>(cartResponse, HttpStatus.OK);
                }

            } else {
                cartRepository.removeProductFromCart(cartRequest.getProductId(), cartRequest.getUserId());
            }
            cartResponse.setData(null);
            cartResponse.setMessage("Success");
            cartResponse.setStatusCode(200);
            return new ResponseEntity<>(cartResponse, HttpStatus.OK);
        } catch (Exception exception) {
            log.error("Unexpected exception while adding product to cart : ", exception);
            CartResponse cartResponse = new CartResponse();
            cartResponse.setData(null);
            cartResponse.setError("Unexpected error");
            cartResponse.setStatusCode(500);
            return new ResponseEntity<>(cartResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
