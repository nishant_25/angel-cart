package com.angel.cart.service;

import com.angel.cart.request.CartRequest;
import com.angel.cart.response.CartResponse;
import org.springframework.http.ResponseEntity;

public interface CartService {

    ResponseEntity<CartResponse> updateProductToCart(CartRequest cartRequest);

}
